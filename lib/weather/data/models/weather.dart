import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

/// For More Accurate Data Using Weather Condition Id
/// https://openweathermap.org/weather-conditions
/// cause we don't know the names previously but we can
/// depend on condition id to get the weather and display icons maybe

enum WeatherConditionStatus {
  unknown,
  thunderstorm,
  drizzle,
  rain,
  snow,
  atmosphere,
  clear,
  cloud,
}

extension on WeatherConditionStatus {
  String getIcon() {
    switch (this) {
      case WeatherConditionStatus.clear:
        return '☀';
      case WeatherConditionStatus.thunderstorm:
        return '⛈';
      case WeatherConditionStatus.drizzle:
      case WeatherConditionStatus.rain:
        return '🌧️';
      case WeatherConditionStatus.cloud:
        return '☁';
      case WeatherConditionStatus.snow:
        return '🌨️';
      case WeatherConditionStatus.atmosphere:
        return '🌫';
      case WeatherConditionStatus.unknown:
      default:
        return '❕';
    }
  }

  List<Color> colors() {
    switch (this) {
      case WeatherConditionStatus.clear:
        return [
          Colors.blueAccent,
          Colors.lightBlueAccent,
        ];
      case WeatherConditionStatus.thunderstorm:
      case WeatherConditionStatus.drizzle:
      case WeatherConditionStatus.rain:
        return [
          Colors.blueGrey.shade800,
          Colors.blueGrey.shade600,
        ];
      case WeatherConditionStatus.cloud:
        return [
          const Color(0xFF689EB8),
          const Color(0xFF689EB8).withOpacity(0.5),
        ];
      case WeatherConditionStatus.snow:
        return [
          const Color(0xFF7F848D),
          const Color(0xFF7F848D).withOpacity(0.5),
        ];
      case WeatherConditionStatus.atmosphere:
        return [
          const Color(0xFF494044),
          const Color(0xFF494044).withOpacity(0.5),
        ];
      case WeatherConditionStatus.unknown:
      default:
        return [
          const Color(0xFF192660),
          const Color(0xFF192660).withOpacity(0.6),
        ];
    }
  }
}

extension WeatherConditionStatusX on int {
  WeatherConditionStatus get weatherStatus {
    if (this > 800) {
      return WeatherConditionStatus.cloud;
    } else if (this == 800) {
      return WeatherConditionStatus.clear;
    } else if (this >= 700) {
      return WeatherConditionStatus.atmosphere;
    } else if (this >= 600) {
      return WeatherConditionStatus.snow;
    } else if (this >= 500) {
      return WeatherConditionStatus.rain;
    } else if (this >= 300) {
      return WeatherConditionStatus.drizzle;
    } else if (this >= 200) {
      return WeatherConditionStatus.thunderstorm;
    } else {
      return WeatherConditionStatus.unknown;
    }
  }
}

extension CelsiusAndKelvin on double {
  double get toKelvin => (this + 273.15).roundToDouble();
  double get toCelsius => (this - 273.15).roundToDouble();
}

class Weather extends Equatable {
  const Weather({
    required this.city,
    required this.temperature,
    required this.condition,
    required this.conditionDescription,
    required this.conditionId,
    required this.lastUpdate,
  });
  final String city;
  final double temperature;
  final String condition;
  final String conditionDescription;
  final int conditionId;
  final DateTime lastUpdate;

  WeatherConditionStatus get weatherConditionStatus =>
      conditionId.weatherStatus;

  List<Color> get colors => weatherConditionStatus.colors();
  String get icon => weatherConditionStatus.getIcon();

  @override
  List<Object?> get props => [
        city,
        temperature,
        condition,
        conditionDescription,
        conditionId,
        lastUpdate,
        weatherConditionStatus,
      ];

  Map<String, dynamic> toMap() {
    return {
      'name': city,
      'temp': temperature,
      'main': condition,
      'description': conditionDescription,
      'id': conditionId,
      'lastUpdate': lastUpdate.toIso8601String(),
    };
  }

  factory Weather.fromMap(Map<String, dynamic> map) {
    return Weather(
      city: map['name'] == null ? 'Unknown' : map['name'] as String,
      temperature: map['temp'] == null ? -1 : map['temp'] as double,
      condition: map['main'] == null ? 'Unknown' : map['main'] as String,
      conditionDescription:
          map['description'] == null ? 'Unknown' : map['description'] as String,
      conditionId: map['id'] == null ? -1 : map['id'] as int,
      lastUpdate: map['lastUpdate'] == null
          ? DateTime.now()
          : DateTime.parse(
              map['lastUpdate'] as String,
            ),
    );
  }
}
