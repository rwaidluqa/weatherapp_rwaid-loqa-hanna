import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:weather_app/common/constants/weather_constants.dart';
import 'package:weather_app/weather/data/models/weather.dart';

class WeatherRepository {
  Future<Weather> fetchWeather(String city) async {
    try {
      final response = await http.get(
        Uri.parse(
            '${WeatherUrls.openWeatherUrl}?q=$city&apikey=${WeatherUrls.apiKey}'),
        headers: {
          'accept-type': 'application/json',
        },
      );

      if (response.statusCode != 200) {
        throw Exception('Something went wrong');
      }

      final data = json.decode(response.body) as Map<String, dynamic>;

      ///Here we have a list of json but i took the first one that's enough
      final weather =
          List<Map<String, dynamic>>.from(data['weather'] as List)[0];
      final temperature = (data['main'] as Map<String, dynamic>)['temp'];
      final name = data['name'] as String;

      return Weather.fromMap(<String, dynamic>{
        ...weather,
        "temp": temperature,
        "name": name,
      });
    } catch (e) {
      throw Exception(e);
    }
  }
}
