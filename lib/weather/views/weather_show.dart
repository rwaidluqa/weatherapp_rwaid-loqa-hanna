import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:weather_app/common/widgets/animation/translate_animation.dart';
import 'package:weather_app/common/widgets/paragraph/body_text.dart';
import 'package:weather_app/common/widgets/paragraph/headline1.dart';
import 'package:weather_app/common/widgets/paragraph/headline2.dart';
import 'package:weather_app/weather/cubit/weather_cubit.dart';
import 'package:weather_app/weather/data/models/weather.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class WeatherShow extends StatelessWidget {
  final Weather? weather;

  const WeatherShow({Key? key, required this.weather}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return SizedBox.expand(
      key: const ValueKey('weather_result'),
      child: Stack(
        key: ValueKey(weather!.city),
        children: [
          Positioned.fill(
            child: AnimatedContainer(
              duration: const Duration(milliseconds: 200),
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: weather!.colors,
                ),
              ),
            ),
          ),
          Positioned(
            right: 10,
            top: 10,
            child: SafeArea(
              child: IconButton(
                onPressed: () {
                  context.read<WeatherCubit>().fetchWeather(weather!.city);
                },
                icon: const Icon(Icons.refresh),
                color: Colors.white,
              ),
            ),
          ),
          SafeArea(
            child: Center(
              child: Padding(
                padding: const EdgeInsets.symmetric(
                  vertical: 25,
                  horizontal: 10,
                ),
                child: Column(
                  children: [
                    ///Animate Using Animation Widgets
                    const SizedBox(height: 10),
                    TranslateAnimation(
                      delay: const Duration(milliseconds: 100),
                      child: BodyText(
                        text: DateFormat(DateFormat.MONTH_WEEKDAY_DAY).format(
                          weather!.lastUpdate,
                        ),
                      ),
                    ),
                    const SizedBox(height: 10),
                    TranslateAnimation(
                      delay: const Duration(milliseconds: 200),
                      child: Headline1(
                        text: DateFormat(DateFormat.HOUR_MINUTE).format(
                          weather!.lastUpdate,
                        ),
                      ),
                    ),
                    const SizedBox(height: 30),

                    TranslateAnimation(
                      delay: const Duration(milliseconds: 300),
                      child: Headline2(
                        text: weather!.city,
                      ),
                    ),
                    const Spacer(),
                    TranslateAnimation(
                      delay: const Duration(milliseconds: 400),
                      child: Headline1(
                        text:
                            '${weather!.temperature.toCelsius.toStringAsFixed(0)}°C',
                      ),
                    ),
                    TranslateAnimation(
                      delay: const Duration(milliseconds: 600),
                      child: Padding(
                        padding: const EdgeInsets.all(10),
                        child: SizedBox(
                          height: size.height * 0.25,
                          width: size.width,
                          child: FittedBox(
                            child: Headline1(
                              text: weather!.icon,
                            ),
                          ),
                        ),
                      ),
                    ),
                    TranslateAnimation(
                      delay: const Duration(milliseconds: 700),
                      child: Headline2(
                        text: weather!.condition,
                      ),
                    ),
                    const SizedBox(height: 6),
                    TranslateAnimation(
                      delay: const Duration(milliseconds: 800),
                      child: BodyText(
                        text: weather!.conditionDescription,
                      ),
                    ),

                    const Spacer(),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
