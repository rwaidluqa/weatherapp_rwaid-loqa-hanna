import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';
import 'package:weather_app/res.dart';

class WeatherInitial extends StatelessWidget {
  const WeatherInitial({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Padding(
        padding: const EdgeInsets.all(8),
        child: Column(
          children: [
            Lottie.asset(Res.weatherAnim),
            const Icon(
              Icons.search,
              size: 80,
              color: Colors.white,
            ),
            const SizedBox(height: 10),
            Text(
              'Start search for a City',
              style: Theme.of(context).textTheme.headline2,
            ),
          ],
        ),
      ),
    );
  }
}
