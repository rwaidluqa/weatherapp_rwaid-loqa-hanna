import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:weather_app/common/widgets/animation/lottie_animation.dart';
import 'package:weather_app/common/widgets/animation/scale_animation.dart';
import 'package:weather_app/res.dart';
import 'package:weather_app/search/views/search.dart';
import 'package:weather_app/weather/cubit/weather_cubit.dart';
import 'package:weather_app/weather/data/repositories/weather_repository.dart';
import 'package:weather_app/weather/views/weather_failure.dart';
import 'package:weather_app/weather/views/weather_initial.dart';
import 'package:weather_app/weather/views/weather_show.dart';

class WeatherPage extends StatelessWidget {
  const WeatherPage({Key? key}) : super(key: key);

  static Widget provideCubit(BuildContext context) {
    return BlocProvider<WeatherCubit>(
      create: (context) => WeatherCubit(
        WeatherRepository(),
      ),
      child: const WeatherPage(),
    );
  }

  @override
  Widget build(BuildContext context) {
    final weatherCubit = context.watch<WeatherCubit>().state;
    return Scaffold(
      body: weatherCubit.weatherStatus.isInitial
          ? const WeatherInitial()
          : weatherCubit.weatherStatus.isFailure
              ? const WeatherFailure()
              : LottieAnimation(
                  showWidget: weatherCubit.weatherStatus.isFetched &&
                      weatherCubit.weather != null,
                  lottieFile: Res.weatherAnim,
                  widget: WeatherShow(
                    weather: weatherCubit.weather,
                  ),
                ),
      floatingActionButton: ScaleAnimation(
        curve: Curves.decelerate,
        child: FloatingActionButton(
          backgroundColor: weatherCubit.weatherStatus.isFetched &&
                  weatherCubit.weather != null
              ? weatherCubit.weather!.colors.first
              : Colors.blueGrey,
          child: const Icon(
            Icons.search,
            color: Colors.white,
          ),
          onPressed: () async {
            final city = await Navigator.of(context).push(SearchPage.route());
            if (city == null) return;
            await context.read<WeatherCubit>().fetchWeather(city);
          },
        ),
      ),
    );
  }
}
