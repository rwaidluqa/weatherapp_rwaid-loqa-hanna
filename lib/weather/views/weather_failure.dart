import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';
import 'package:weather_app/res.dart';

class WeatherFailure extends StatelessWidget {
  const WeatherFailure({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Padding(
        padding: const EdgeInsets.all(8),
        child: Column(
          children: [
            Lottie.asset(Res.weatherAnim),
            const Icon(
              Icons.error,
              size: 80,
              color: Colors.white,
            ),
            const SizedBox(height: 10),
            Text(
              'Something Went Wrong\nPlease check your connection and try again.',
              style: Theme.of(context).textTheme.headline3,
              textAlign: TextAlign.center,
            ),
          ],
        ),
      ),
    );
  }
}
