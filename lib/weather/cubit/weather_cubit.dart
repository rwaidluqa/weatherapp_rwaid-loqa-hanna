import 'package:equatable/equatable.dart';
import 'package:hydrated_bloc/hydrated_bloc.dart';
import 'package:weather_app/weather/data/models/weather.dart';
import 'package:weather_app/weather/data/repositories/weather_repository.dart';

part 'weather_state.dart';

class WeatherCubit extends HydratedCubit<WeatherState> {
  WeatherCubit(this._weatherRepository) : super(const WeatherState()) {
    if (state.weather != null) {
      fetchWeather(state.weather!.city);
    }
  }
  final WeatherRepository _weatherRepository;

  Future<void> fetchWeather(String city) async {
    emit(state.copyWith(weatherStatus: WeatherStatus.loading));
    try {
      print(city);
      final weather = await _weatherRepository.fetchWeather(city);
      emit(
        state.copyWith(weather: weather, weatherStatus: WeatherStatus.fetched),
      );
    } catch (e) {
      emit(state.copyWith(weatherStatus: WeatherStatus.failure));
    }
  }

  @override
  WeatherState? fromJson(Map<String, dynamic> json) {
    return WeatherState.fromMap(json);
  }

  @override
  Map<String, dynamic>? toJson(WeatherState state) {
    return state.toMap();
  }
}
