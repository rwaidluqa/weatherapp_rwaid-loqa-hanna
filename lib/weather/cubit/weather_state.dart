part of 'weather_cubit.dart';

enum WeatherStatus { initial, loading, fetched, failure }

extension WeatherStatusX on WeatherStatus {
  bool get isInitial => this == WeatherStatus.initial;
  bool get isLoading => this == WeatherStatus.loading;
  bool get isFetched => this == WeatherStatus.fetched;
  bool get isFailure => this == WeatherStatus.failure;
}

class WeatherState extends Equatable {
  const WeatherState({
    this.weatherStatus = WeatherStatus.initial,
    this.weather,
  });
  final WeatherStatus weatherStatus;
  final Weather? weather;

  @override
  List<Object> get props => [
        weatherStatus,
        if (weather != null) weather!,
      ];

  WeatherState copyWith({
    WeatherStatus? weatherStatus,
    Weather? weather,
  }) {
    return WeatherState(
      weatherStatus: weatherStatus ?? this.weatherStatus,
      weather: weather ?? this.weather,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'weatherStatus': weatherStatus.name,
      'weather': weather?.toMap(),
    };
  }

  factory WeatherState.fromMap(Map<String, dynamic> map) {
    return WeatherState(
      weatherStatus: WeatherStatus.values.firstWhere(
        (element) => element.name == map['weatherStatus'],
        orElse: () => WeatherStatus.initial,
      ),
      weather: map['weather'] == null ? null : Weather.fromMap(map['weather']),
    );
  }
}
