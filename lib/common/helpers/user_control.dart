import 'package:flutter/material.dart';
import 'package:weather_app/common/widgets/animation/scale_animation.dart';

class UserControl {
  static void showAlert(
    BuildContext context, {
    required String title,
    required String content,
    VoidCallback? onOkTapped,
  }) {
    showDialog<bool>(
      context: context,
      builder: (context) {
        return ScaleAnimation(
          curve: Curves.decelerate,
          duration: const Duration(milliseconds: 400),
          child: AlertDialog(
            buttonPadding: EdgeInsets.zero,
            title: Text(
              title,
              style: const TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 24,
              ),
            ),
            content: Text(
              content,
              style: const TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 20,
              ),
            ),
            actions: [
              TextButton(
                style: TextButton.styleFrom(primary: Colors.black),
                onPressed: () {
                  if (onOkTapped != null) {
                    onOkTapped();
                  }
                  Navigator.of(context).pop();
                },
                child: const Text(
                  'Ok',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 16,
                  ),
                ),
              ),
            ],
          ),
        );
      },
    );
  }

  static void showSnackBar(BuildContext context, String text) {
    ScaffoldMessenger.of(context)
      ..hideCurrentSnackBar()
      ..showSnackBar(
        SnackBar(
          content: Text(text),
        ),
      );
  }
}
