import 'package:flutter/material.dart';

class Headline1 extends StatelessWidget {
  final String text;

  const Headline1({Key? key, required this.text}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    final fontSize = (size.width * 0.065).clamp(18.0, 38.0);
    return Text(
      text,
      style: Theme.of(context).textTheme.headline1!.copyWith(
            fontSize: fontSize,
          ),
    );
  }
}
