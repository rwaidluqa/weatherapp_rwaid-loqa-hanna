import 'package:flutter/material.dart';

class Headline2 extends StatelessWidget {
  final String text;

  const Headline2({Key? key, required this.text}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    final fontSize = (size.width * 0.055).clamp(18.0, 38.0);
    return Text(
      text,
      style: Theme.of(context).textTheme.headline2!.copyWith(
            fontSize: fontSize,
          ),
    );
  }
}
