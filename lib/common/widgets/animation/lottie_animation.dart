import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';

class LottieAnimation extends StatefulWidget {
  const LottieAnimation({
    Key? key,
    required this.lottieFile,
    required this.widget,
    this.showWidget = false,
  }) : super(key: key);

  final String lottieFile;
  final Widget widget;
  final bool showWidget;
  @override
  _LottieAnimationState createState() => _LottieAnimationState();
}

class _LottieAnimationState extends State<LottieAnimation> {
  late final ValueNotifier<bool> showWidgetNotifier;
  @override
  void initState() {
    super.initState();
    showWidgetNotifier = ValueNotifier(widget.showWidget);
  }

  @override
  void dispose() {
    showWidgetNotifier.dispose();
    super.dispose();
  }

  @override
  void didUpdateWidget(covariant LottieAnimation oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (oldWidget.showWidget != widget.showWidget) {
      Future.delayed(
        const Duration(milliseconds: 1000),
        () {
          if (mounted) {
            showWidgetNotifier.value = widget.showWidget;
          }
        },
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return ValueListenableBuilder(
      valueListenable: showWidgetNotifier,
      builder: (BuildContext context, bool showWidget, Widget? child) {
        return AnimatedSwitcher(
          duration: const Duration(milliseconds: 350),
          child: showWidget
              ? child
              : SizedBox.expand(
                  key: const ValueKey('animation_widget'),
                  child: Lottie.asset(
                    widget.lottieFile,
                    repeat: true,
                  ),
                ),
        );
      },
      child: widget.widget,
    );
  }
}
