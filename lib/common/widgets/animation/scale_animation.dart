import 'package:flutter/material.dart';

class ScaleAnimation extends StatefulWidget {
  const ScaleAnimation({
    Key? key,
    this.duration = const Duration(milliseconds: 600),
    this.delay = Duration.zero,
    this.curve = Curves.easeInOut,
    this.alignment = Alignment.center,
    required this.child,
  }) : super(key: key);

  final Widget child;
  final Duration duration;
  final Duration delay;
  final Curve curve;
  final Alignment alignment;

  @override
  State<ScaleAnimation> createState() => _ScaleAnimationState();
}

class _ScaleAnimationState extends State<ScaleAnimation> {
  ValueNotifier<bool> showItemNotifier = ValueNotifier(false);

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance!.addPostFrameCallback((timeStamp) {
      Future.delayed(widget.delay, () {
        showItemNotifier.value = true;
      });
    });
  }

  @override
  void dispose() {
    showItemNotifier.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ValueListenableBuilder<bool>(
      valueListenable: showItemNotifier,
      builder: (BuildContext context, bool show, Widget? child) {
        return show ? child! : Container();
      },
      child: TweenAnimationBuilder<double>(
        tween: Tween<double>(begin: 1, end: 0),
        duration: widget.duration,
        curve: widget.curve,
        builder: (context, value, child) {
          return Transform.scale(
            scale: 1 - value,
            alignment: widget.alignment,
            child: child,
          );
        },
        child: widget.child,
      ),
    );
  }
}
