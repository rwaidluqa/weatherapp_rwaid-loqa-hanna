import 'package:flutter/material.dart';

class TranslateAnimation extends StatefulWidget {
  const TranslateAnimation({
    Key? key,
    this.duration = const Duration(milliseconds: 700),
    this.delay = Duration.zero,
    this.axis = Axis.vertical,
    this.offset = 100,
    this.curve = Curves.easeInOut,
    this.alignment = Alignment.center,
    required this.child,
  }) : super(key: key);

  final Widget child;
  final Duration duration;
  final Duration delay;
  final double offset;
  final Axis axis;
  final Curve curve;
  final Alignment alignment;

  @override
  State<TranslateAnimation> createState() => _TranslateAnimationState();
}

class _TranslateAnimationState extends State<TranslateAnimation> {
  ValueNotifier<bool> showItemNotifier = ValueNotifier(false);

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance!.addPostFrameCallback((timeStamp) {
      Future.delayed(widget.delay, () {
        showItemNotifier.value = true;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return ValueListenableBuilder<bool>(
      valueListenable: showItemNotifier,
      builder: (BuildContext context, bool show, Widget? child) {
        return show ? child! : Container();
      },
      child: TweenAnimationBuilder<double>(
        tween: Tween<double>(begin: 1, end: 0),
        duration: widget.duration,
        curve: widget.curve,
        builder: (context, value, child) {
          return Opacity(
            opacity: (1 - value).clamp(0.0, 1.0),
            child: Transform.translate(
              offset: widget.axis == Axis.horizontal
                  ? Offset(widget.offset * value, 0)
                  : Offset(0, widget.offset * value),
              child: child,
            ),
          );
        },
        child: widget.child,
      ),
    );
  }
}
