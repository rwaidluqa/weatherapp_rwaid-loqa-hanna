import 'package:flutter/material.dart';
import 'package:weather_app/common/widgets/animation/translate_animation.dart';

class SearchPage extends StatefulWidget {
  const SearchPage({Key? key}) : super(key: key);

  static PageRoute route() {
    return MaterialPageRoute(builder: (context) {
      return const SearchPage();
    });
  }

  @override
  State<SearchPage> createState() => _SearchPageState();
}

class _SearchPageState extends State<SearchPage> {
  late final TextEditingController textEditingController;

  @override
  void initState() {
    super.initState();
    textEditingController = TextEditingController();
  }

  @override
  void dispose() {
    textEditingController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final outlineBorder = OutlineInputBorder(
      borderRadius: BorderRadius.circular(5),
    );

    return Scaffold(
      appBar: AppBar(
        title: const Text('Search For A City'),
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(
          vertical: 25,
          horizontal: 20,
        ),
        child: Column(
          children: [
            TranslateAnimation(
              axis: Axis.horizontal,
              child: TextField(
                controller: textEditingController,
                decoration: InputDecoration(
                  prefixIcon: const Icon(
                    Icons.location_city,
                    color: Colors.white70,
                  ),
                  label: const Text('City Name'),
                  labelStyle: Theme.of(context).textTheme.bodyText2,
                  hintText: 'eg. California',
                  border: outlineBorder,
                  focusedBorder: outlineBorder.copyWith(
                    borderSide: const BorderSide(
                      color: Colors.white70,
                    ),
                  ),
                  enabledBorder: outlineBorder,
                  fillColor: Colors.grey.shade800,
                  filled: true,
                ),
              ),
            ),
            const SizedBox(height: 20),
            TranslateAnimation(
              axis: Axis.horizontal,
              delay: const Duration(milliseconds: 200),
              child: SizedBox(
                height: 40,
                width: double.infinity,
                child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    primary: Theme.of(context).cardColor,
                  ),
                  onPressed: () async {
                    FocusScope.of(context).unfocus();

                    ///So that the keyboard is hidden
                    await Future.delayed(const Duration(milliseconds: 200));
                    Navigator.of(context).pop(textEditingController.text);
                  },
                  child: const Text('Search'),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
